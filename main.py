import os

import pandas as pandas
from requests import Session
from tqdm import tqdm

session = Session()
session.get('https://xinit.ru/')


def get_imei_info(_imei: str) -> str:
    if not imei:
        return
    cookies = {
        '_ga': 'GA1.2.1657260705.1617795548',
        '_gid': 'GA1.2.1370814878.1617891518',
        'tzr_id': 'api08-556181b8-f8c0-4630-806b-f7774c46571e',
        'tzr_permission': 'yes',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Host': 'xinit.ru',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15',
        'Accept-Language': 'ru',
        'Referer': 'https://xinit.ru/imei/12390005731060',
        'Connection': 'keep-alive',
    }
    response = session.get(
        'https://xinit.ru/api/imei/{imei}'.format(imei=_imei),
        cookies=cookies,
        headers=headers
    )
    try:
        info = ''
        for json_data in response.json() or []:
            if len(json_data.get('info')) >= len(info):
                info = json_data.get('info')

        return info
    except Exception as error:
        print(error)


for name in os.listdir('input'):
    if name.endswith('.txt'):
        data = list()
        for imei in tqdm(open('input/' + name).read().split('\n')):
            data.append(dict(
                imei_data=get_imei_info(imei) or 'Ну удалось установить',
                imei=imei
            ))
        writer = pandas.ExcelWriter("results/" + name.replace('.txt', '.xlsx'), engine='xlsxwriter')
        pandas.DataFrame(data).to_excel(writer, 'Data')
        writer.save()
