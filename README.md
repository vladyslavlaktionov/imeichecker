# IMEI Checker

Пайтон проект, для масовой проверки устройств по IMEI 

### Перед установкой

```
apt update && apt upgrade -y
apt install python3-venv git -y
```

### Установка

```
git clone https://gitlab.com/vladyslavlaktionov/imeichecker
cd imeichecker
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Запуск

Для проверки imei, создайте файл с расширением .txt в папке input и поместите в него список imei
Для запуска скрипта выполните следующую команду 
```
python main.py
```

После завершения выполнения скрипта в папке results появиться файл с расширение xlsx
